import { basePath } from "../api/config";
import { baseSystem } from "../api/config";

export async function CallAPIGet(method) {
  const url = `${basePath}${method}`;

  try {
    const rawResponse = await fetch(url, {
      method: "GET",
      mode: "cors",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": `${baseSystem}`,
        "Access-Control-Allow-Methods": "OPTIONS, GET, POST, PUT, PATCH, DELETE",
        "Access-Control-Allow-Headers": "X-PINGOTHER, Content-Type, Authorization",
      },
    });

    const content = await rawResponse.json();
    return content;
  } catch (error) {
    return error;
  }
}

export async function CallAPIPost(method, jsonData) {
  const url = `${basePath}${method}`;

  const rawResponse = await fetch(url, {
    method: "POST",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": `${baseSystem}`,
      "Access-Control-Allow-Methods": "OPTIONS, GET, POST, PUT, PATCH, DELETE",
      "Access-Control-Allow-Headers": "X-PINGOTHER, Content-Type, Authorization",
    },
    body: jsonData,
  });

  const content = await rawResponse.json();
  return content;
}

export async function CallAPI(type, method, jsonData) {
  try {
    const url = `${basePath}${method}`;
    const typeFormat = type.toUpperCase();

    const rawResponse = await fetch(url, {
      method: typeFormat,
      mode: "cors",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": `${baseSystem}`,
        "Access-Control-Allow-Methods": "OPTIONS, GET, POST, PUT, PATCH, DELETE",
        "Access-Control-Allow-Headers": "X-PINGOTHER, Content-Type, Authorization",
      },
      body:
        typeFormat === "POST" || typeFormat === "PUT" //|| typeFormat === "DELETE"
          ? jsonData
          : "",
    });

    const content = await rawResponse.json();
    return content;
  } catch (error) {
    return error;
  }
}
