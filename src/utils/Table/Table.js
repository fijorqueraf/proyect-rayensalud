import React from "react";
import { Table as TableAntd } from "antd";

export default function Table(props) {
  const { columns, data, loadingTable } = props;

  return (
    <TableAntd
      pagination={{ defaultPageSize: 5 }}
      loading={loadingTable}
      columns={columns}
      dataSource={data}
      style={{ width: "100%" }}
    />
  );
}
