import React, { useState, useEffect } from "react";
import { Form, Button, Row, Col, Tooltip, Table } from "antd";
import { SearchOutlined, DeleteOutlined } from "@ant-design/icons";
import Table from "../../utils/Table";

export default function TutorialListForm(props) {
    const { insertTutorial, updateTutorial, deleteTutorialById, tutorial, update } = props;
    const [inputs, setInputs] = useState({ search: "" });

    const onChange = (e) => {
        e.preventDefault();
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value,
        });
    };

    useEffect(() => {
        setInputs({
            ...inputs,
            id: tutorial.id,
            nombre: tutorial.nombre,
            profesor: tutorial.profesor,
            materia: tutorial.materia,
            fecha: tutorial.fecha
        });
    }, []);


    const onChangeDate = (e) => {
        let fechaDesde = moment(e).format("YYYY-MM-DD");

        setInputs({
            ...inputs,
            fechas: fechaDesde,
        });
    };

    const onFinish = () => {
        if (inputs.id == 0) {
            insertTutorial(inputs);
        } else { 
            updateTutorial(inputs);
        }
    }

    const deleteTutorial = () => {
        deleteTutorialById(inputs.id);
    }

    return (
        <Form id="form-search" name="complex-form" onFinish={onFinish}>
            <Row>
                <Col span={6}>
                    <Form.Item label="Titulo"
                        name="nombre"
                        placeholder="titulo"
                        onChange={onChange}
                    ></Form.Item>
                </Col>
                <Col span={6}>
                    <Form.Item label="Profesor"
                        name="profesor"
                        placeholder="titulo"
                        onChange={onChange}
                    ></Form.Item>
                </Col>
                <Col span={6}>
                    <Form.Item label="Materia"
                        name="materia"
                        placeholder="titulo"
                        onChange={onChange}
                    ></Form.Item>
                </Col>
                <Col span={6}>
                    <Form.Item label="Fecha" style={{ marginBottom: 0 }}>
                        <Form.Item
                            style={{
                                display: "inline-block",
                                width: "calc(50% - 5px)",
                                marginRight: 8,
                            }}
                        >
                            <RangePicker
                                format="DD-MM-YYYY"
                                picker="day"
                                style={{ width: 250, marginLeft: 15 }}
                                name="fechas"
                                value={inputs.fecha}
                                onChange={onChangeDate}
                            />
                        </Form.Item>
                    </Form.Item>
                </Col>
            </Row>
            <Row>
                <Col span={24}>

                    <Tooltip title={!update ? "Guardar" : "Modificar"} color="green">
                        <Button
                            type="primary"
                            htmlType="submit"
                            style={{ marginLeft: "3%" }}
                        >
                            <SearchOutlined />
                        </Button>
                    </Tooltip>
                    <Tooltip title="Elimiar" color="green">
                        <Button
                            type="primary"
                            htmlType="submit"
                            onClick={deleteTutorial}
                            style={{ marginLeft: "3%" }}
                        >
                            <DeleteOutlined />
                        </Button>
                    </Tooltip>
                    <Tooltip title="Volver" color="green">
                        <Button type="button" onClick={() => (window.location.href = "/admin/tutorial-list")}>
                            <RollbackOutlined />Volver
                        </Button>
                    </Tooltip>
                </Col>
            </Row>
        </Form>
    )
}