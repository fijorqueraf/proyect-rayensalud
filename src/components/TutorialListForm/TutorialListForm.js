import React, { useState, useEffect } from "react";
import { Form, Input, Select, Button, DatePicker, Row, Col, Tooltip, notification, Table } from "antd";
import { SearchOutlined, DeleteOutlined } from "@ant-design/icons";

export default function TutorialListForm(props) {
    const { columns, data, getTutorialListBySearch, deleteTutorialAll } = props;
    const [inputs, setInputs] = useState({
        nombre: "",
        profesor: "",
        materia: "",
        fecha: ""
    });
    const onChange = (e) => {
        e.preventDefault();
        setInputs({
            ...inputs,
            [e.target.name]: e.target.value,
        });
    };

    const onFinish = () => {
        getTutorialListBySearch(inputs.search);
    }

    return (
        <Form id="form-search" name="complex-form" onFinish={onFinish}>
            <Row>
                <Col span={24}>
                    <Form.Item label="Buscar"
                        name="search"
                        placeholder="Buscar por titulo"
                        onChange={onChange}
                    ></Form.Item>
                </Col>
            </Row>
            <Row>
                <Col span={24}>
                    <Table columns={columns} data={data}></Table>
                </Col>
            </Row>
            <Row>
                <Col span={24}>
                    <Tooltip title="Buscar tutorial" color="green">
                        <Button
                            type="primary"
                            htmlType="submit"
                            style={{ marginLeft: "3%" }}
                        >
                            <SearchOutlined />
                        </Button>
                    </Tooltip>
                    <Tooltip title="Elimiar Todo" color="green">
                        <Button
                            type="primary"
                            htmlType="submit"
                            onClick={deleteTutorialAll}
                            style={{ marginLeft: "3%" }}
                        >
                            <DeleteOutlined />
                        </Button>
                    </Tooltip>
                </Col>
            </Row>
        </Form>

    )
}