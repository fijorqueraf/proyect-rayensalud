import TutorialList from "../pages/TutorialList";
import ManteinerTutorial from "../pages/ManteinerTutorial";
import ManteinerTutorialForm from "../components/ManteinerTutorialForm";

const routes = [
    {
        path: "/admin",
        component: Formulario,
        exact: false,
        routes: [
            {
                path: "/admin/tutorial-list",
                component: TutorialList,
                exact: true,
            },
            {
                path: "/admin/manteiner-tutorial",
                component: ManteinerTutorial,
                exact: true,
            },
            {
                path: "/admin/manteiner-tutorial-form",
                component: ManteinerTutorialForm,
                exact: true,
            }
        ]
    }
];

export default routes;