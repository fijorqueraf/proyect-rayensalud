import { Card } from "antd";
import React, { useState, useEffect } from "react";
import {  CallAPIGet, CallAPI } from "../../utils/metodCommonAPI";

import TutorialListForm from "../../components/TutorialListForm";

export default function TutorialList() {
    const [tutList, setTutList] = useState([]);
    const [tutorial, setTutorial] = useState([]);

    useEffect(() => {
        getTutorialList();
    }, []);

    const columns = [
        {
            title: "",
            dataIndex: "id",
            key: "id",
            sorter: (a, b) => a.id - b.id
        },
        {
            title: "Nombre",
            dataIndex: "nombre",
            key: "nombre",
            ellipsis: true,
            sorter: (a, b) => a.nombre.localeCompare(b.nombre),
        },
        {
            title: "Profesor",
            dataIndex: "profesor",
            key: "profesor",
            sorter: (a, b) => a.profesor.localeCompare(b.profesor),
        },
        {
            title: "Materia",
            dataIndex: "materia",
            key: "materia",
            sorter: (a, b) => a.materia.localeCompare(b.materia),
        },
        {
            title: "Fecha",
            dataIndex: "fecha",
            key: "fecha",
            sorter: (a, b) => a.Fecha.localeCompare(b.Fecha),
            render: (date) => <span>{moment(date).format("DD-MM-YYYY")}</span>,
        },
        {
            title: "Acciones",
            dataIndex: "nombre",
            key: "nombre",
            render: (id) => {
                return (
                    <>
                        <Tooltip title="Modificar Tutorial" color="green">
                            <Button
                                type="primary"
                                shape="round"
                                onClick={() => { window.location.href = `../admin/manteiner-tutorial-form?search=${id}&accion=update` }}
                                style={{ marginRight: 16 }}>
                                <EyeOutlined />
                            </Button>
                        </Tooltip>
                        <Tooltip title="Eliminar Tutorial" color="green">
                            <Button
                                type="danger"
                                shape="round"
                                onClick={() => { deleteTutorialAll(); }}
                                style={{ marginRight: 16 }}>
                                <FormOutlined />
                            </Button>
                        </Tooltip>
                    </>
                );
            },
        },
    ];

    const getTutorialList = async () => {
        const method = `/tutorials`;
        const response = await CallAPIGet(method);
        setTutList(response);
    }

    const getTutorialListBySearch = async (search) => {
        const method = `/tutorials?description=${search}`;
        const response = await CallAPIGet(method);
        setTutList(response);
    }

    const deleteTutorialAll = async () => {
        const method = `/deletetutorials`;
        const response = await CallAPI("DELETE", method, "");
        getTutorialList();
    }
    return (
        <div className="tutorial">
            <Card title="Tutorial" bordered={false} style={{ width: "100%" }}>
                <TutorialListForm
                    columns={columns}
                    data={tutList}
                    getTutorialListBySearch={getTutorialListBySearch}
                    deleteTutorialAll={deleteTutorialAll}>
                </TutorialListForm>
            </Card>
        </div>
    )
}