import { Card } from "antd";
import React, { useState, useEffect } from "react";
import { CallAPIPost, CallAPIGet, CallAPI } from "../../utils/metodCommonAPI";

import ManteinerTutorialForm from "../../components/ManteinerTutorialForm";

export default function TutorialList() {
  const [tutorial, setTutorial] = useState([]);
  const [update, setUpdate] = useState(false);

  useEffect(() => {
    let url = new URL(window.location.href);
    let accion = url.searchParams.get("accion");
    if (accion == "update") {
      let search = url.searchParams.get("search");
      getTutorialListBySearch(search);
      setUpdate(true);
    }
  }, []);

  const getTutorialListBySearch = async (search) => {
    const method = `/tutorials?description=${search}`;
    const response = await CallAPIGet(method);
    setTutorial(response);
  }

  const insertTutorial = async (data) => {
    const method = `/createtutorial`;

    const jsonData = JSON.stringify({
      nombre: data.nombre,
      profesor: data.profesor,
      materia: data.materia,
      fecha: data.fecha
    });

    const response = await CallAPIPost(method, jsonData);
    setTutorial(response);
  }

  const updateTutorial = async (data) => {
    const method = `/createtutorial/${data.id}`;

    const jsonData = JSON.stringify({
      nombre: data.nombre,
      profesor: data.profesor,
      materia: data.materia,
      fecha: data.fecha
    });

    const response = await CallAPI("PUT", method, jsonData);
    setTutorial(response);
  }

  const deleteTutorialById = async (id) => {
    const method = `/deletetutorial/${id}`;
    const response = await CallAPI("DELETE", method, "");
    getTutorialList();
  }

  return (
    <div className="tutorial">
      <Card title="Tutorial" bordered={false} style={{ width: "100%" }}>
        <ManteinerTutorialForm
          insertTutorial={insertTutorial}
          updateTutorial={updateTutorial}
          deleteTutorialById={deleteTutorialById}
          tutorial={tutorial}
          update={update}
        >
        </ManteinerTutorialForm>
      </Card>
    </div>
  )
}